---
title: "Uniques"
linkTitle: "Uniques"
type: docs
description: >
  Locate unique hidden items to collect
---

Minecraft Left2Die has several unique items for players to search for and collect. All uniques are enchanted. This list may not be complete.

Uniques are tied to their owner (the first player to touch them). Only the owner can interact with them. As such, Uniques cannot be traded or sold.

You can view a list of your collected uniques with `/unique list`. Uniques are automatically added when you pick them up. You can view your total unique count with `/stats` or by hovering over your name in [chat]({{< ref "/wiki/general/chat" >}}). The top unique collectors are viewable via `/statstop uniques`.

Per the [rules]({{< ref "/wiki/general/rules" >}}), do not ask for the location of uniques in chat. You can, however, privately collaborate with other players in private messages or clan chat so long as it's not a one way trade (ie all involved players need to be working to find uniques and equally trading, not just one player seeking and telling). 

{{% alert title="Warning" color="warning" %}}
Do not ask for or discuss the location of uniques in chat.
{{% /alert %}}


## Map Uniques

These uniques can be found hidden throughout the maps.

### MegaMap

The MegaMap contains the City and Town maps with a much larger landscape surrounding them.

#### City

- Allium
- Azure Bluet
- Bloo's Flower Of Death
- Blue Orchid
- Blue's boots
- Chicken Lawyer's Briefcase[^chickenlawyersbriefcase]
- Dandelion
- Drinking from the Sun
- Earthmelon
- Firemelon
- Gravity Hammer
- Ladies Don't Poop
- Last Letter
- Lime Banner
- Love Potion
- Man Next Door's Door
- Men's Toliet Paper
- Orange Tulip
- Pablo's Threads
- Pizza Pan
- Poggers[^poggers]
- Ralen's Torment[^ralenstormet]
- Rose Bush
- The Dicer
- The Key
- Tobo's Magic Stick[^tobosmagicstick]
- tnt's TNT[^tntstnt]
- Vanishing Mushroom[^vanishingmushroom]
- Watermelon
- Windmelon
- Wondermelon
- Zim's Axe

#### Town

- A _ _ _ Ate My Homework
- Blaze Rod
- "Cow" Spawn Egg
- JellyBeanRed
- Lucky Rabbit Foot
- _ _ _ Poop
- Safety Torch[^safetytorch]
- _ _ _ _ _ Coffee
- Starbucks Coffee
- Tobo's Powerdirt
- Zim's Money Tree
- Zim's Shiny Fern
- Zombie Friend Flesh[^zombiefriendflesh]

#### The Tower

- \#9
- "Chemistry" Set[^chemistryset]
- Death Switch
- Hoppy the Hopper
- Railroad to Nowhere
- Red Sheep Fuzz
- Torch of Darkness
- Torch of Light
- Zim's Lily Pad

#### Rest of MegaMap

- a potato[^apotato]
- Golden Sword
- He was #1[^hewasnumber1]
- The Holy Grail[^theholygrail]
- Jayzan's Apple
- Jayzan's Diamond[^jayzansdiamond]
- Llama Llama Duck[^llamallamaduck]
- Larry the Pumpkin
- Lost Heart
- Magic Balloon
- Mini Sauron
- Nemo
- Not Nemo
- Observatory's Puzzle is Easy
- Power Arrow
- Ralen's Head[^ralenshead]
- Resourceful Textile
- Rotten Potato[^rottenpotato]
- Santa Protection
- Sekrit Kart
- Squirrel's Acorn
- Squishy
- Tobo's Windbreaker
- Torpedo
- Victor
- Wrong Way Feldman[^wrongwayfeldman]
- XBow
- zenith's Leisure Suit
- zenith's Pantaloons
- zenith4183's Prot IV Head
- Zim's Prickly Dead Shrub
- Zim's Blade

### Themed Maps

#### Aquashock

- Aquashock Helmet
- The Badger's Death Bow
- Big Daddy
- Bloo's Bonafied Blackskin
- Bloo's One Trick Pony
- Das Kaufinator
- Das Kaufinator The Second[^daskaufinatorthesecond]
- The Engineers Wrench
- JohnBPKC's Toothpick
- The Poetry Of Zangoozer
- Refined Plasma Shell
- Zenith Blade
- Zenith's Safe Key

#### The End

- Smaug
- Smaug's Breath
- Voodoo Shulker[^voodooshulker]
- Vorlianth's Breath
- Vorlianth the Stubborn

#### Islands

- Captain's Whip
- The Cure
- Green's Immortal Blood
- Killer
- Not the XBow[^nottheexbow]
- Tobos Toxic Tazer
- Vic's Magical Mushroom
- Zim's Silky Potato[^zimssilkypotato]

#### Nether

- Arctic's Snowy Snowball
- Captain Hook
- Dusty Spoon
- Hard Yogurt
- Lava Bucket
- Lemonz' Welding Rod
- Never Waste your Diamonds on a Hoe
- NTN
- Speed Clock
- Souleater
- Strangely Hidden Sponge
- Tent Stake
- Zim's Prickly Dead Shrub
- Nether Wart

#### Satz

- Beheaded
- Breastplate Of The Bloss
- Breeches Of The Bloss
- Brogues Of The Bloss
- Danii's Scythe
- Jay's Secret Decoder Ring

#### Time

- Ahead of time
- Alarm clock
- American Flag
- A whale of a good time
- Balloon Clock
- Cuckoo Clock
- Digital Clock
- Floppy Disc
- Grandfather Clock
- Grandmother Clock
- Having the time of my life
- ice cube
- In the nick of time
- King Tut's Egg
- "Let them eat cake!"
- Living on borrowed time
- Long time no see
- Once upon a time
- Pocketwatch
- Skeleton Clock
- Stopwatch
- Time is money
- Time to kill
- Tnt's Hedge Clippers
- Wristwatch

#### Tiny

- Ak-Zim7
- Be Our Guest
- Brick's Brutal Brick
- Brick's Brick 2.0
- Chemical X
- Chemical Y
- Chemical Z
- Dust Bunny
- Enchantress
- Everlasting Item
- Fence Gate
- Fly Droppings
- Follower not a Leader
- G-R92Badger
- L2-19BlossDaBoss
- Led Zeppelin - Mothership
- Long John's Log
- Mjornir
- Molly's Poking Stick
- Nin's Hot Cookie Pan
- Odin's Eye
- One Whole Wheat Penny
- R12-BlooCheese
- Salt's Comic Sans
- Sliq's Inferno Flyswatter
- Sooohigh's Magical Shroom
- Strange Machine
- Sword of Gryffindor
- Tyrannosaurus Egg
- Widow Maker
- World Edit Wand
- Xray Piece Uno
- Xray Piece Dos
- Xray Piece Tres
- Xray Piece Quatro
- Zen's _ _ _ _ _ _ _ _ Cereal
- Zombie Blocker
- Zy's MIDI Controller

#### Titanic

- 2:20 am
- Badger's Frozen Jellyfish[^badgersfrozenjellyfish]
- Blanket
- Jelly's Pie
- Jay's Handcuffs
- Titanic First Class Menu
- Titanic Third Class Menu
- Titanic lifeboat

#### Zender

- Page 1
- Page 2
- Page 3
- Page 4
- Page 5
- Page 6
- Page 7
- Page 8
- Missing Truck Wheel


## Boss Uniques

Bosses will drop a unique item among their other drops when killed.

### Megamap

- Bacon Bit
- Bacon Sword
- Crown of Skele
- Demon Sword
- Draco's Diamond
- Eye of the Spider Queen
- Ghast Hoe[^ghasthoe]
- The Head of Zerg
- Inspected by #9
- Lupen's Chew Toy
- Power Sword
- Sheep Axe
- Sheep Fuzz
- Sword of the Arthropods
- Sword of Chickney[^swordofchickney]
- The Teeth of Lupen
- Zim's Bow[^zimsbow]
- Zerg Powder

### Tiny

- Ted's Tear

### Aquashock

- Lemon
- Squiddy Ink

### Zender

- Pageless Book


## Voting Uniques

### Milestone Uniques

- Blade of the Bloss

### Crate Uniques

#### Vote Party Crate

- Fatal's Pants
- Force of a Thousand Gods
- I don't want it, you take it
- Max's Magical Organizer
- Reaper's Tears
- Takao's Missing Piston
- Throwing Pickle
- Voter Cake
- Voter Sword
- Whoopsie
- Zy's Corona

#### Top Weekly Voter Crate

- Draco's Head
- Dynamite
- Fluffy's Head
- Fortune Cookie
- In Case of Emergency
- I voted and only got a potato
- Lost in the Woods
- Message in a Bottle
- Pet Rock
- Popcorn
- Soon to be Potato Chips
- Tadpole
- Witch's Broken Broom

#### Top Monthly Voter Crate

- Boomerang
- Future Diamond
- Grey Matter
- Log
- Sushi
- The Watcher
- Nuclear Warhead

#### School Uniques Crate

- Art Project
- Backpack
- Binder
- Eraser
- Fountain Pen
- Ink Blot
- Locker
- Lunch Tray
- My Dog Ate My Homework
- Mobile Microwave
- Notepad
- Pencil
- Pencil Sharpener
- Teacher's Apple

#### Summer Uniques Crate

- Bucket o' Sunscreen
- Pool Floatie
- Popsicle
- Sand Castle
- Sea Water
- Sun Glasses
- Tiki Torch


## Quest Uniques

- The Quest for the Holy Grail

## Shop Uniques

Occasionally a unique will appear in the shop or market. These are normally a one time thing so obtaining it after sale will likely be impossible unless the buyer trades or sells it later.

- Fluffy
- Companion Cube
- Cubed Nemo
- I can't Believe it's not Budder
- Potato Lemon
- Notch's Almighty Bedrock

## Holiday Uniques

These were given out one time at a holiday. The only way to get them after the event is by trading with whoever has one.

### Easter

- Bionic Bunny
- Bugs Bunny
- Easter Bunny
- The Energizer Bunny
- Hazel
- The Jackalope
- Roger Rabbit
- Trix

## Removed Uniques

The following uniques are no longer in the maps and may or may not make a return.

- Cindy's Obsession
- Fusion's Shield
- Irish's Lucky Charm
- Joshy's Cart Of Destruction
- Joshy's Super Sponge

### Retired Map Uniques

#### Olympus

- a potato
- Appendage Cutter
- Badger's Head Banger
- Badger's Melon Baller
- Bloo's Blue Hair Dye
- Blue's Blue Block
- Budder Helmet
- Budder Chestplate
- Budder Leggings
- Budder Pants
- Brick's Stairs
- Bybloss Egg
- Cheese =D
- Dead Cow
- Don't Tell Blue
- Eagle's Molted Feather
- Elektro Squeed
- Just Got Killed by a Book
- MK14
- Ninja's Cookie
- *poke*
- Scare Crow
- Spongebob
- This Machine
- XBow
- You just got killed by a hoe
- Zim's Compass[^zimscompass]

### Retired Quest Uniques

- Excalibur
- Old Man's Tea Doilies
- Pete's Meatloaf

### Retired Voting Uniques

#### Retired Milestone Uniques

- a potato
- XBow

#### Retired Crate Uniques

- Beach Towel
- Broken Wings
- Fanta
- Fruit Punch
- Potato[^potato]
- Spawn Fluffy
- Water Balloon[^test]

## Trivia

[^chickenlawyersbriefcase]: **Chicken Lawyer's Briefcase:** Inside joke referring to a chicken zenith spawned on zyxwa2000's plot
[^poggers]: **Poggers:** Head of the second player to beat [The Tower]({{< ref "/wiki/gameplay/activities/puzzles#tower-puzzle" >}}), Fataltryhardz.
[^ralenstormet]: **Ralen's Tormet:** Placed in the puzzle that gave Ralen, the first player to beat [The Tower]({{< ref "/wiki/gameplay/activities/puzzles#tower-puzzle" >}}), a harder time than The Tower to entice him to actually beat it.
[^tobosmagicstick]: **Tobo's Magic Stick:** The first unique ever hidden and the only one to exist until the release of the [Town]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/town" >}}) map.
[^tntstnt]: **tnt's TNT:** Hidden by tnt378 in a build that was abandoned, never finished, and hidden away by another builder, but was still found and confused people.
[^vanishingmushroom]: **Vanishing Mushroom:** Reference to the giant mushroom forest that used to be in this unique's hiding spot.
[^safetytorch]: **Safety Torch:** Reference to the Tobuscus song, [Safety Torch](https://www.youtube.com/watch?v=upxzaVMhw8k)
[^zombiefriendflesh]: **Zombie Friend Flesh**: Inside joke to a zombie that a group of players led on a tour around the Town map on stream until it was accidentally killed. Located where the zombie died.
[^chemistryset]: **"Chemistry" Set:** Reference to the show Breaking Bad.
[^apotato]: **a potato:** Sharp VI sword intended to be used in PvP to give the death message that player was slain by a potato, an item frequently given out at random by zenith.
[^hewasnumber1]: **He was #1:** [Spongebob](https://www.youtube.com/watch?v=RS7mk-UtdjQ) reference
[^theholygrail]: **The Holy Grail**: Took over seven years to be found. Reference to Monty Python and The Holy Grail.
[^jayzansdiamond]: **Jayzan's Diamond:** The first difficult to obtain unique added.
[^llamallamaduck]: **Llama Llama Duck:** Reference to [The Llama Song](https://www.youtube.com/watch?v=zRozKfYGFVc)
[^ralenshead]: **Ralen's Head:** First player to beat [The Tower]({{< ref "/wiki/gameplay/activities/puzzles#tower-puzzle" >}}).
[^rottenpotato]: **Rotten Potato:** Placed in the original location of the 'a potato' unique.
[^wrongwayfeldman]: **Wrong Way Feldman:** Gilligan's Island reference.
[^daskaufinatorthesecond]: **Das Kaufinator The Second:** Bybloss forgot the original location of Das Kaufinator, so thinking it would never be found (or that it wasn't there anymore at all), he created a second one.
[^voodooshulker]: **Voodoo Shulker:** Yet another inside joke referring to a stubborn shulker box on zyxwa2000's plot that would not allow items to be taken out of it.
[^nottheexbow]: **Not the XBow:** Located in the original location of the Xbow.
[^zimssilkypotato]: **Zim's Silky Potato:** Originally placed as an X-ray trap until too many players found it on accident via TNT and spreading the word of it's existence.
[^badgersfrozenjellyfish]: **Badger's Frozen Jellyfish:** A reference to TheDeathBadger's first attempt at building the iceberg looking like a frozen jellyfish.
[^ghasthoe]: **Ghast Hoe:** Reference to [Jose the Happy Ghast's](bosses.html) garden in the [Nether]({{< ref "/wiki/gameplay/worlds/zombieworlds/themed/nether" >}}) map.
[^swordofchickney]: **Sword of Chickney:** The spelling of 'chickney' is a reference to the [I Love Chickneys](https://www.youtube.com/watch?v=AFV_9BV9voU) song.
[^zimsbow]: **Zim's Bow:** Is a copy of the bow that zenith (zim) used at the time that [King Skele]({{< ref "/wiki/gameplay/activities/bosses" >}}) was created.
[^zimscompass]: **Zim's Compass:** Reference to the fact that zenith (zim) is regularly holding a compass.
[^potato]: **Potato:** Was a typo of the Popcorn unique's name.

