---
title: "Speedclock"
linkTitle: "Speedclock"
type: docs
description: >
  Use the speedclock for a speedy escape
---

<img src="/img/wiki/Speedclock_recipe.png"  width="158px" height="184px" />

The Speedclock is a special item that when left clicked will give the player a five second speed III boost, increasing by a second with each [rank]({{< ref "/wiki/gameplay/features/ranks" >}}). This is useful when trying to get out of a tight situation. Each speedclock has twelve uses and comes with a 60 second cooldown between uses. The speedclock is created with a clock surrounded with sugar (see recipe image to right).

Using the speedclock comes with a slight amount of risk. There is a 10% chance that the clock will malfunction and instead of giving speed, give slowness II.
