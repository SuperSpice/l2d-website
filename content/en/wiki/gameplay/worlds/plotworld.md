---
title: "Plotworld"
linkTitle: "Plotworld"
weight: 2
type: docs
description: >
  The safe world
---

Plotworld is the safe area of L2D where you can build a house and store your loot.

Plotworld spawn is the location of the enchantment tables (you can of course also make your own).

<img src="/img/wiki/Plotworld.png"  width="300px" height="170px" />

## Commands

### Plot Claiming

| Command     | Description                                                                              | Cost       |
| ----------- | ---------------------------------------------------------------------------------------- | ---------- |
| /plot auto  | Claim the next available plot.                                                           | $30        |
| /plot claim | Claim a blank plot.                                                                      | $30        |
| /plot buy   | Buy a plot that is for sale. You cannot go over your plot number limit by buying a plot. | Sale Price |

### Warp Commands

| Command            | Description                                                                                                                                                                                 |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| /pw                | Go to plotworld spawn                                                                                                                                                                       |
| /plot sethome      | Sets your home point on your plot. This is where /p h will take you.                                                                                                                        |
| /plot h            | Teleport you to your plot. This command can only be used while in plotworld.                                                                                                                |
| /plot h #          | Teleport to a particular plot. This command can only be used while in plotworld. Only donators and ranks with multiple plots can use this command.                                          |
| /plot visit <name> | Teleport you to another player's plot if you are added to that plot. This command can only be used while in plotworld. Donators can teleport to any plot without needing to be added to it. |

### Plot Permissions

{{% alert title="Warning" color="warning" %}}
Be careful who you add to your plot. Added players can grief and loot your chests. If they are added, then this is allowed per the rules.
{{% /alert %}}

| Command              | Description                                                                                                  |
| -------------------- | ------------------------------------------------------------------------------------------------------------ |
| /plot add <name>     | Add a player to your plot. They will be able to break blocks and access chests only when you are online.     |
| /plot trust <name>   | Allow a player access to your plot even when you are offline.                                                |
| /plot remove <name>  | Remove a player from your plot (reverse of /plot add and /plot deny).                                        |
| /plot untrust <name> | Remove player from trust status. They will still be able to access your plot when you are online.            |
| /plot deny <name>    | Deny a player access to your plot.                                                                           |
| /plot info           | Shows ownership and everyone added and denied from your plot or whatever plot you are currently standing on. |
| /plot list           | Lists every plot you have perms on.                                                                          |

### Plot Maintenance

| Command             | Description                                            |
| ------------------- | ------------------------------------------------------ |
| /plot biome <biome> | Change the biome of your plot. Donators only.          |
| /plot clear         | Delete everything on your plot. This cannot be undone. |
| /plot delete        | Clear and delete your plot. This cannot be undone.     |

### Plot Merging

Only donators can merge or request a plot merge. Once merged, all commands will act against the entire merged plot.

{{% alert title="Warning" color="warning" %}}
If you do /p clear or /p delete on a merged plot the ENTIRE MERGED PLOT will be wiped.
{{% /alert %}}

| Command                 | Description                                                                                                                                                                      | Cost |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---- |
| /plot merge <direction> | Merge your plot to the plot in the direction you specify. If you do not own the plot, the owner will be sent an invite. If they accept, the merged plot will contain two owners. | $500 |
| /plot unlink            | Unmerges a merged plot. This unmerges everything and anything where the roads were will be destroyed. 	                                                                     |      |

### Plot Flags

Plot flags are available only to donators (except protection).

| Command                          | Description                                                                                                       | Required Rank |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------------- | ------------- |
| /plot protect<br>/plot unprotect | Protect and unprotect a plot from expiration.                                                                     | ZombieSlayer  |
| /plot flag set price #           | Sets a plot for sale.                                                                                             | Infected+     |
| /plot music                      | Play music when entering a plot.                                                                                  | Survivor+     |
| /plot flag set greeting <msg>    | Display a message for players entering the plot. Follow the chat rules or you will lose this command permanently. | Survivor+     |
| /plot flag set farewell <msg>    | Display a message for players leaving a plot. Follow the chat rules or you will lose this command permanently.    | Survivor+     |
| /plot flag set titles true       | Displays a title over the plot when a player enters it.                                                           | Survivor+     |
| /plot flag set notify-enter true | Notifies you when a player enters your plot.                                                                      | Survivor+     |
| /plot flag set notify-leave true | Notifies you when a player leaves your plot.                                                                      | Survivor+     |
| /plot flag remove <flag-name>    | Removes a flag from the previous flag commands.                                                                   |               |

## Plot Moves

Donators get one free plot move to move their original, single plot to an area where multiple plots can be together and merged.

## Plot Expiration

Plots expire after three months of no block activity (placing and breaking blocks, chest opening does NOT count) or logging in. Donators can protect their plots from deletion when they expire. Once reset, the plot cannot be restored.

## Limitations

Plotworld has the following limitations to prevent server lag and abuse:

- There is a limit of 10 animals per plot.
- There is a limit of 3 vehicles (boats, minecarts).
- There is a limit of 50 total entities (mobs + vehicles + item frames).
- Natural mob spawning is disabled. You will need to find mob eggs.
- Mobs that wander into the roads will die.
- Redstone will only be active so long as the plot is occupied. As soon as you leave the plot or log off, the redstone will turn off.
- Leaves will not decay. Donators have access to /deltree to quickly remove leaves.
- TNT is not enabled on plots.
