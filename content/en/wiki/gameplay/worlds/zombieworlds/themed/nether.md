---
title: "Nether"
linkTitle: "Nether"
type: docs
description: >
  A Nether biome world
---

Find your way through the natural maze like features of the nether.

{{< cardpane >}}
{{< card header="**Nether**">}}

<center>{{< figure src="/img/wiki/L2d_nether.png" width="308px" height="334px" >}}</center>

**First Release:** July 5, 2014

**First Retirement:** May 22, 2019

**Second Release:** January 1, 2020

**Size:** 350x350

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- zenith4183
- BlueCharm
- TheDeathBadger

**Schematics Creators:**

- Unknown

{{< /card >}}
{{< /cardpane >}}

The Nether map was created in response to several players asking for access to the nether. It was created in about two days.

The map was originally intended to be a puzzle map. As such, there are several puzzles and odd clues hidden throughout the map. The map contains several hidden areas and looping tunnels making it a difficult map to navigate and find hidden loot.

Because it was originally intended as a puzzle map, the Nether map does not allow the use of enderpearls.

## Trivia

- The Nether map is a real chunk of the nether. It was a slice of the Nether from zenith's first single player map he ever player with Minecraft. The copied chunk was taken from a previously un-generated section to allow for Nether quartz to be generated.
- The first version of the map only had zenith working on it. Other staff started adding to it much later.
- One area of the map contains an odd, rectangular box up against the edge of the map. Much to many a player's despair upon finding a way in, there is nothing in this box. It was merely a chunk generation error that was blocked off rather than attempting to fix it.

