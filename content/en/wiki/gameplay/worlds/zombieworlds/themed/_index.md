---
title: "Themed"
linkTitle: "Themed"
type: docs
description: >
  The rotating themed maps
---

The themed maps rotate every restart and are accessible via `/themed` or warp signs at the spawn points. [Donators]({{< ref "/wiki/contribute/donate" >}}) can vote from a list of three potential themed maps to be chosen for the next restart via the `/votethemed` command.
