---
title: "End"
linkTitle: "End"
type: docs
description: >
  The End biome world
---

One of the most requested map suggestions, the End map is a literal piece of the End realm (dragon not included....yet).

{{< cardpane >}}
{{< card header="**End**">}}

**First Release:** April 17, 2016

**First Retirement:** May 22, 2019

**Second Release:** January 1, 2020

**Size:** 350x350

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- zenith4183
- BlueCharm

{{< /card >}}
{{< /cardpane >}}

The End map was created in response to player requests to be able to access the End. This map is a direct copy/paste from a naturally generated End world with minimal changes made to it.

## Trivia

- The [uniques]({{< ref "/wiki/gameplay/activities/uniques" >}}) uniques in this map were all named after famous movie/book dragons.
