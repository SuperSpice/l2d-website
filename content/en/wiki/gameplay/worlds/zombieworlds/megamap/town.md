---
title: "Town"
linkTitle: "Town"
type: docs
description: >
  The Western spawn region of Megamap
---

Small town that loves their Starbucks. Can you find all 30+ of them?

{{< cardpane >}}
{{< card header="**Town**">}}

<center>{{< figure src="/img/wiki/L2d_town.png" width="308px" height="334px" >}}</center>

**First Release:** July 29, 2013

**First Retirement:** April 26, 2014

**Second Release:** June 4, 2014

**Size:** 500x500

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- zenith4183
- toboein
- OhPistolPete
- PuarZ
- NetCaptive
- mctiger300
- fvqu
- jayzan
- tnt378

{{< /card >}}
{{< /cardpane >}}

The Town map was the revived Left2Die's second map. It was originally created to entirely replace the [City]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/city" >}}) map and later came to be available along side it.

Unlike the City map which was built largely out of schematics, the Town map was done entirely by the build team. The majority of the map was built by zenith4183 and toboein. The map took roughly a week to complete. 


## Trivia

- When originally released, the football field and the housing area were PvP zones. Later on, only the Football Field was PvP. The field was later made non-PvP after too many incidents of new players being lured into it.
- The map is riddled with inside jokes and back stories.
	- There were originally 33 Starbucks in the Town map. They were created as a joke to go along with Lewis Blacks [Starbucks](https://www.youtube.com/watch?v=Mb7qDfIzQRk) sketch. On the map's opening, L2D had a contest involving finding all 33 of the StarBucks.
	- In the Football Field, there is a quote from a conversation located on the back of one of the band stands that occurred during the building of the Town map.
	- The UFO was originally the spawn of another server zenith had previously run.
	- In the map, there's a tornado, several meteors hurtling towards the ground, a UFO attack, and fires. These are all disasters from SimCity 3000, a favourite game of zenith's.
- During building of the map, the build server crashed resulting in a three hour rollback. Several buildings had to be rebuilt as a result, such as the Target.
- The train station was built by zenith while toboein was afk. She returned ten minutes later to find herself in a completed train station. Zenith had always claimed to have no building ability and until that point had only ever done terrain work and underground, puzzle builds. She didn't let him off the hook after that.
- When asking toboein what to build, tnt378 got the response "meatloaf and poop". As a result, tnt378 built a series of buildings spelling out the word "MEATLOAF" and had planned to make the highway spell out "POOP". OhPistolPete "accidentally" destroyed the builds.
- There used to be an underground cache of a large amount of quartz blocks with a random set of wood doors located under the bank. None of the build team can remember how it got there. It was later removed in late 2015.
- When put in the MegaMap, it was intentionally pasted into the terrain in a (poor) attempt to make the town look overgrown. Most of the map beyond the strip mall was also removed as it did not fit well in the area.  This has since been cleaned up.
- The map received some cleanup and improvements in March 2015.

