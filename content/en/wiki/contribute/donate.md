---
title: "Donating"
linkTitle: "Donating"
type: docs
description: >
  Donating helps pay the bills and keep the server running
---

Donating helps keep the server running. If you are financially able to do so and enjoy Minecraft Left2Die, please consider donating.

## Donating

Both methods will ultimately lead to the website, but you can start the process in game if you like. If you receive any complications while completing a donation, please message staff in-game or on our [Discord](https://discord.gg/VERWnRf) server about it.

### In Game

1. Type /buy
2. Click Ranks --> Left2Die Ranks
3. We have two donator ranks: Infected and Survivor. If you donate for Infected first, the amount will automatically be deducted from the Survivor price should you donate for it later.
4. A link will be provided for you in chat. Click that to proceed on the website.
5. Provide your details and click the Stripe button.
6. Pay via Stripe.
7. Once the payment is processed, you will automatically receive your rank.

### Online

Visit the [Donation store](https://donate.minecraftleft2die.com)
