---
title: "Privacy Policy"
linkTitle: "Privacy Policy"
weight: 4
type: docs
description: >
  The Left2Die Privacy Policy
---

## General

- Minimal personal information is collected by Minecraft Left2Die itself. Third parties may collect more personal information. See the third parties section below.
- While every effort is made to ensure what information collected directly by Minecraft Left2Die stays secure, this cannot be 100% guaranteed due to the unencrypted and public nature of communication on a Minecraft server, the possibility of unknown vulnerabilities, the possibility of volunteer staff members gone rogue, among other things. It is purely the responsibility of the player to not reveal any personal information via the in game chat (public or private messages), Discord, or any other official MinecraftLeft2Die communication channels. that they would not want to risk becoming public.
- Personal information is not knowingly collected from children 13 years of age or younger.

## Main Website

- Cookies may be stored purely for session purposes. No tracking or analytic cookies are used.
- Basic visitor data is stored in logs for approximately two weeks. These logs contains typical data stored by web servers including IP address, visited URL, referrer URL, and User Agent.
- Additional logging is also stored by the CDN provider.  See third parties below for their privacy policy.

## Donation Page

- The following information is required to make a donation on the donation site and is shared with Minecraft Left2Die:
	- A Minecraft username
	- Email Address
- The following additional details are shared with Minecraft Left2Die by the payments processor:
	- First and last name
	- Address
	- Email address associated with the card
	- Credit card issuer, expiration date, and last four digits of the PAN
- Full credit card payment details (full PAN and CVV) are never shared with Minecraft Left2Die or the donation site.  This information is only stored by the payment processor.
- The above details are not used for any additional purpose by Minecraft Left2Die other than for the processing of the donation.  Personal details are not stored anywhere other than on the donation store and payment processor's websites.  See third party privacy policies below for how they handle this personal information.
- The only donation information stored by Minecraft Left2Die is the donation amount and date for records keeping.  Usernames are also effectively stored in the logs via the in-game notification alerts following a donation.


## Minecraft Server

- Logs are stored containing all chat (public and private), commands executed, usernames, IP addresses, player UUID, player statistics, and any other gameplay related data.
- Like all unencrypted forms of chat, there should be no expectation of privacy from chat on the server. Other players can view global chat and server staff are capable of seeing private messages for the purposes of enforcing chat rules. Players accept full responsibility for any personal information they choose to share on the server and any consequences that result from it
- Players should assume that all chat is public, even direct messages.  Anyone viewing a message in-game could potentially screenshot and share any chat messages they are capable of viewing.
- Server log files are stored indefinitely as disk space allows.
- The server is hosted on a shared VPN hosting provider.  Their security practices can have a direct impact on the security of the data stored on the Minecraft Left2Die VPS instance regardless of the security of the VPS instance itself managed by Minecraft Left2Die.  The Minecraft Left2Die VPS instance itself is highly secured following the Center for Internet Security's (CIS) Linux benchmarks and other security best practices.

## Discord

- View the third parties section below for Discord's privacy policy.
- Minecraft Left2Die cannot access or moderate direct messages on Discord.
- Discord does not delete messages and as such are stored indefinitely by them unless deleted by the user.  Deleted messages are stored in a private auditing channel viewable only by the head admin (and presumably Discord staff).  As Discord does not provide a method for mass message deletion, these deleted message audit logs are also stored indefinitely.
- While the Discord server is currently locked down to players only, users should assume that all messages are public as anyone in the server can copy/paste or screenshot and share messages.  These channels could also potentially become fully public in the future.
- Player Minecraft server login/logout details containing their username and skin face are stored in a public notifications channel which can be viewed by anyone, player or not.  Similar to the above, this information is stored indefinitely.
- Several third party Discord bots used for Moderation and other features have access to read all chat messages in the Discord channels.  See the third parties section below for their privacy policies (for the bots that have them).


## Third Parties

- [OVH Privacy Policy](https://us.ovhcloud.com/legal/privacy-policy) - Server Hosting
- [Cloudflare Privacy Policy](https://www.cloudflare.com/privacypolicy/) - Content Delivery Network (CDN) and DDoS Mitigation
- [Crafting Store Privacy Policy](https://craftingstore.net/legal/privacy) - Donation Store
- [Stripe Privacy Policy](https://stripe.com/privacy) - Donation Payment Processor
- [GitLab Privacy Policy](https://about.gitlab.com/privacy/) - Website Source Code and Editing
- [Discord Privacy Policy](https://discord.com/privacy) - Discord Chat Server
- [Mee6 Privacy Policy](https://www.iubenda.com/privacy-policy/61412973/full-legal) - Discord Bot
- [DynoBot Privacy Policy](https://www.iubenda.com/privacy-policy/21925808/legal) - Discord Bot
