---
title: "Staff"
linkTitle: "Staff"
type: docs
description: >
  Server staff and how to contact them
---

## Current Staff

### Admins

| Skin                                                                           | Name                   |
|:------------------------------------------------------------------------------:|:----------------------:|
| ![](https://crafatar.com/avatars/7f11b263-be19-4db3-baca-266a181f44b3?size=50) | zenith4183 (aka "zim") |
| ![](https://crafatar.com/avatars/575f09de-ebe5-4d94-a68d-3b97a3017488?size=50) | zyxwa2000              |

### Moderators

| Skin                                                                           | Name       |
|:------------------------------------------------------------------------------:|:----------:|
| ![](https://crafatar.com/avatars/f148d066-e8fa-421f-aa87-11971fa7bfb9?size=50) | MadamBunny |
| ![](https://crafatar.com/avatars/f4251932-44ed-4d0c-b691-c45c3751362e?size=50) | Evdy       |

### Chat Moderators

| Skin                                                                           | Name            |
|:------------------------------------------------------------------------------:|:---------------:|
| ![](https://crafatar.com/avatars/326cdecb-0a1d-47c0-a46d-e3a3f1703715?size=50) | AmazingLemonz   |
| ![](https://crafatar.com/avatars/bde1b993-ee28-43c5-a245-ed74dc36275f?size=50) | MightyMasterMax |


## Contacting Staff

If a staff member is not currently online, they can typically be reached in our [Discord](https://discord.gg/JWZXt8V).

To report another player to staff, use `/report <player> <msg>` in game.

### What to Report

* **Report hackers.** Please do not yell out "HACKER" in chat. If you can get screenshots or video, include a link in the report.
* **Report glitches or other server problems.** We don't catch all glitches on our own. Exploiting a glitch without reporting it could result in a ban.  Report these on our [Discord](https://discord.gg/JWZXt8V) *privately* to staff.
	* Some staff have their message settings set to friends only.  It's easier to state in #general that you've found a glitch to report and we'll start the private message with you to get the details.
* **Report illegal PvP.**
* **Report advertising.**

### What not to Report

* **Grief** -  L2D is a griefable server. We will not repair grief of any kind. Do not add people to your claims that you cannot absolutely trust.
* **Stealing or scamming** - Both of these activities are allowed.  Be careful who you trust to trade with.
* **Lost items on death** - Don't want to lose items on death? Then don't die.

## Applying for Staff

If you think you have what it takes to be staff, you can apply by clicking [here](http://bit.ly/l2dstaffapp). Read below for requirements, expectations, and what we look for when choosing staff. The application is more of a formality to let us know you are interested. What we see in game is the real application.  The majority of players that have been promoted to staff positions over the years never filled out an application.

{{% alert title="Warning" color="warning" %}}
**If you are not staff then DO NOT behave as if you are staff!**

Helping out other players is okay and expected, but do not threaten to get a player muted, banned, or any other form of punishment for which you do not have the power to issue yourself or harass players that do not listen to you.
{{% /alert %}}

### Chat Moderator

* All new moderating staff start out as Chat Moderators. We do not promote straight to Moderator.
	* Requirements:
		* Must be at least 13 years old or have the maturity of that level or higher.
		* Must be an active player for at least a month.
		* Must not have had any serious issues with breaking the server rules.
	* Expectations:
		* Chat mods are responsible for maintaining control of chat and knowing what is and is not okay behaviour.
		* Chat mods are responsible for the muting and un-muting of players who are a major disturbance to chat stability.
		* Chat mods are not moderators and do not have ban powers. Any issue that may be a bannable offense should be taken up with a higher ranked staff member and not with players themselves.
		* Chat mods are able to play the game as normal players with the exception of scamming and stealing from other players. Players should be able to trust staff.
	* What we look for:
		* Chat mods are chosen based on their helpfulness in chat and on how active they are on the server.

### Moderator

* Requirements:
	* Must be at least 16 years old or have the maturity of that level or higher.
	* Must have been a Chat mods for at least three months, often times longer depending on the need for new Moderators.
* Expectations:
	* Mods have the same expectations as Chat mods. The difference is that Mods have ban power. Bans are reserved largely for hacking, advertising, illegal PvP (normally temp bans), and severe disturbances to the server.
	* Mods handle player reports submitted via the report command.
	* Mods are able to play the game as normal players so long as they do not abuse their extra powers to gain a personal advantage or to boost the advantage of a particular individual. As with Chat mods, Mods are expected not to scam or steal from other players.
* What we look for:
	* Dedication to the server, maturity, activeness, and trustworthiness.

## Previous Staff

### Admins

| Skin                                                                           | Name     | Skin                                                                           | Name      |
|:------------------------------------------------------------------------------:|:--------:|:------------------------------------------------------------------------------:|:---------:|
| ![](https://crafatar.com/avatars/b3ba2e02-e789-4f52-b6d2-8d9d9a835473?size=50) | toboein  | ![](https://crafatar.com/avatars/9c61c9da-0223-4898-ab0f-9453e1ce54a1?size=50) | BlueCharm |

### Moderators

| Skin                                                                           | Name           | Skin                                                                           | Name           |
|:------------------------------------------------------------------------------:|:--------------:|:------------------------------------------------------------------------------:|:--------------:|
| ![](https://crafatar.com/avatars/d1c92fe3-b567-4505-97fe-340e0e73c7bc?size=50) | GreenChaChing  | ![](https://crafatar.com/avatars/380e78a8-876c-4320-a98c-98096fc272e8?size=50) | Josh_Reid      |
| ![](https://crafatar.com/avatars/403dfe9c-04e4-4f42-a4c1-9004128e1ca0?size=50) | Br0ken0ut      | ![](https://crafatar.com/avatars/2ed5aeee-0d54-4aa2-989a-05defd4d0fc4?size=50) | TheDeathBadger |
| ![](https://crafatar.com/avatars/395366dd-47a4-4163-8d4a-e6cd81a9d89a?size=50) | VictoriaAucoin | ![](https://crafatar.com/avatars/9df76b97-576d-4bc6-93a2-1c58e56cd63b?size=50) | JohnBPKC       |
| ![](https://crafatar.com/avatars/323c85fb-810a-4503-80c1-1ffcd7671fa4?size=50) | fvqu           | ![](https://crafatar.com/avatars/ec774ad6-17d5-48b9-9971-b62be6d8e577?size=50) | Bybloss        |
| ![](https://crafatar.com/avatars/25483c98-38e3-46aa-b538-3a7794cab0ee?size=50) | tnt378         | ![](https://crafatar.com/avatars/06961644-45a1-49a0-bdd8-8377ff7c0021?size=50) | JaysonBond     |

### Chat Moderators

| Skin                                                                           | Name           | Skin                                                                           | Name           |
|:------------------------------------------------------------------------------:|:--------------:|:------------------------------------------------------------------------------:|:--------------:|
| ![](https://crafatar.com/avatars/216f003e-f788-43fe-8575-20bb5abea46f?size=50) | bkauf2         | ![](https://crafatar.com/avatars/60036294-eb1e-4d7a-af6a-d268f89bb238?size=50) | EagleTheory    |
| ![](https://crafatar.com/avatars/7adb159d-49ec-4c20-a3c2-2c1b0ed950fb?size=50) | zangoozer      | ![](https://crafatar.com/avatars/5b5c8a65-9d17-4812-8eaa-9dca143161f4?size=50) | hammy607       |
| ![](https://crafatar.com/avatars/aa678479-64eb-492c-a263-c2c8567af2f7?size=50) | ktb094         | ![](https://crafatar.com/avatars/1164ce99-e5a8-4888-ae26-6fa29e644a68?size=50) | eggo123        |
| ![](https://crafatar.com/avatars/16bbb26c-a2d4-45dd-9e9c-5bb36fbee8f2?size=50) | brickalator    | ![](https://crafatar.com/avatars/4a555275-0d53-410f-8e02-ba3c4278ece1?size=50) | KillerSkull    |
| ![](https://crafatar.com/avatars/fc76ac8b-871e-4749-8047-290ce1b02c2c?size=50) | Vitalbra       | ![](https://crafatar.com/avatars/c6d816b6-40a7-4c88-babe-ce083e7d055e?size=50) | MoonFeather    |
| ![](https://crafatar.com/avatars/9068ef55-b29a-4ea8-81e5-f0de9f615aea?size=50) | ninjasparten07 | ![](https://crafatar.com/avatars/0ebc9f70-c8f8-44fa-80a8-dad50b1a05a0?size=50) | Jeffie         |


### Builders

| Skin                                                                           | Name           | Skin                                                                           | Name            |
|:------------------------------------------------------------------------------:|:--------------:|:------------------------------------------------------------------------------:|:---------------:|
| ![](https://crafatar.com/avatars/b3ba2e02-e789-4f52-b6d2-8d9d9a835473?size=50) | toboein        | ![](https://crafatar.com/avatars/d03e8446-cc92-4aab-a869-be2dfc036c64?size=50) | PuarZ           |
| ![](https://crafatar.com/avatars/e3d3416b-61c5-4a6f-8609-d5fc9580806c?size=50) | M4gicman       | ![](https://crafatar.com/avatars/23470cf7-bd04-4cc9-9722-fd2267a15896?size=50) | OhPistolPete    |
| ![](https://crafatar.com/avatars/99175deb-5243-4f3e-ab3a-1efc6953cd82?size=50) | NetCaptive     | ![](https://crafatar.com/avatars/f7cb1d65-52d8-4f20-8077-6051379e66e2?size=50) | mctiger300      |
| ![](https://crafatar.com/avatars/323c85fb-810a-4503-80c1-1ffcd7671fa4?size=50) | fvqu           | ![](https://crafatar.com/avatars/a8084769-ca8a-4fc8-9c10-ff16003a0952?size=50) | Sabbing         |
| ![](https://crafatar.com/avatars/769a6dbe-fde9-4a42-8535-3f10cccf28d0?size=50) | Jayzan         | ![](https://crafatar.com/avatars/395366dd-47a4-4163-8d4a-e6cd81a9d89a?size=50) | VictoriaAucoin  |
| ![](https://crafatar.com/avatars/9df76b97-576d-4bc6-93a2-1c58e56cd63b?size=50) | JohnBPKC       | ![](https://crafatar.com/avatars/1164ce99-e5a8-4888-ae26-6fa29e644a68?size=50) | eggo123         |
| ![](https://crafatar.com/avatars/8e29bc0e-a3c5-4fe5-916e-6239d9342cee?size=50) | Jacki          | ![](https://crafatar.com/avatars/2ed5aeee-0d54-4aa2-989a-05defd4d0fc4?size=50) | TheDeathBadger  |
| ![](https://crafatar.com/avatars/e61b6f58-21e9-4b76-95e8-22a9557720d4?size=50) | creeperbuy     | ![](https://crafatar.com/avatars/726d8aef-26d3-40c3-a68e-fa4f10e5acae?size=50) | Hawkushin       |
| ![](https://crafatar.com/avatars/1ed31bf9-be86-4d07-af4d-f568f5444639?size=50) | Archer114897   | ![](https://crafatar.com/avatars/94d96391-3dcf-46f3-9eab-d2b35556b623?size=50) | JellyBeanRed    |
| ![](https://crafatar.com/avatars/fc76ac8b-871e-4749-8047-290ce1b02c2c?size=50) | Vitalbra       | ![](https://crafatar.com/avatars/7f2dfc7f-e2d5-448c-9a5b-131ca01cb07a?size=50) | Rextec_         |
| ![](https://crafatar.com/avatars/3b66c648-1dec-4a10-be0f-0c69d5351823?size=50) | Too_Awkward    | ![](https://crafatar.com/avatars/c05c915a-5490-43b7-b6ee-74ecf12f1a4c?size=50) | apigwithabat    |
| ![](https://crafatar.com/avatars/a0d92c3f-6c47-4059-915c-05d41065ac79?size=50) | Chxco          | ![](https://crafatar.com/avatars/f4251932-44ed-4d0c-b691-c45c3751362e?size=50) | Evdy            |
| ![](https://crafatar.com/avatars/42f895ca-8641-4bd1-8002-d7ecce96cce1?size=50) | khatherine     | ![](https://crafatar.com/avatars/e4eeae6b-3390-424e-8763-361e4107f0ed?size=50) | Vohvin          |
| ![](https://crafatar.com/avatars/df14d970-5468-4c37-8bc4-b2eabd6b3fbc?size=50) | karina9687     | ![](https://crafatar.com/avatars/824e18c7-d2e5-47f2-8d87-b63ac487847e?size=50) | KraythDan       |
| ![](https://crafatar.com/avatars/9068ef55-b29a-4ea8-81e5-f0de9f615aea?size=50) | ninjasparten07 | ![](https://crafatar.com/avatars/47d2c871-5112-4e57-9962-2a02808c0cba?size=50) | ProfessorFusion |
| ![](https://crafatar.com/avatars/851c0cc1-45cb-49d4-b94d-02cf79627766?size=50) | Creation467    | ![](https://crafatar.com/avatars/403dfe9c-04e4-4f42-a4c1-9004128e1ca0?size=50) | br0ken0ut       |
| ![](https://crafatar.com/avatars/ec774ad6-17d5-48b9-9971-b62be6d8e577?size=50) | Bybloss        | ![](https://crafatar.com/avatars/25483c98-38e3-46aa-b538-3a7794cab0ee?size=50) | tnt378          |
| ![](https://crafatar.com/avatars/6e597e21-a380-4028-b89e-4f0978daa1e5?size=50) | BullCraft74    | ![](https://crafatar.com/avatars/095d5e5d-af3d-40b1-b08e-0cab7627d8a6?size=50) | Mapleheartt     |
| ![](https://crafatar.com/avatars/41aa8a10-bc31-490f-b318-2ab94fd1a410?size=50) | _Takao         | ![](https://crafatar.com/avatars/9c61c9da-0223-4898-ab0f-9453e1ce54a1?size=50) | BlueCharm       |
| ![](https://crafatar.com/avatars/f148d066-e8fa-421f-aa87-11971fa7bfb9?size=50) | MadamBunny     | ![](https://crafatar.com/avatars/c6d816b6-40a7-4c88-babe-ce083e7d055e?size=50) | Moonfeather     |
