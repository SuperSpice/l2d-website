---
title: "Administrators"
linkTitle: "Administrators"
type: docs
weight: 1
robotsdisallow: true
exclude_search: true
---

## Responsibilities

- Have near full access to everything on the server.
- Act as the highest tier for any issues that have escalated above other staff with only the Head Admin being above all other administrators.
- Can have a variety of responsibilities with managing and developing the server on both the front end and back end.

## Guidelines

- The same as with Junior Administrators.
