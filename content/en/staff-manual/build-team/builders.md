---
title: "Builders"
linkTitle: "Builders"
type: docs
weight: 2
robotsdisallow: true
exclude_search: true
---

## Responsibilities

* Builds the L2D maps per the guidance of the Architect or Head Admin.
* Builders will be assigned a map or maps to which they will have access.
* There are multiple builder roles.  Some builders may have more than one role.
	* New Builders/Touchwork Builders
		* Do not have WorldEdit and mostly do touchwork, fine tuning and other minor aspects for the map.
	* WorldEdit Builders
		* Have access to WorldEdit commands to perform larger, more complex portions of the map build.
	* Looters
		* Only a select few builders will have the ability to place and fill chests and other item containers.
* Because builders have such privileged access, those that are not actively building for a month will be removed from the rank.  This is also the most scrutinized rank to ensure nothing is happening that should not be.

## Guidelines

### All Tiers

* While the architect will have control over the larger vision, do not rely on them to tell you everything to do.  Take whatever the theme for the map is, and build something along that theme.  Builds may later be modified, moved, or used elsewhere, but rarely ever destroyed to reduce lost effort.
* Avoid using excessive amounts of blocks derived from ores (diamond, gold, iron, emerald, etc).  They should be used very sparingly and not used as a primary building block for any build.
* Everything is tracked, logged, and monitored.  Do not take advantage of the position.
* Don’t put enderchests in PvP.
* Use the sandbox area of /spawn to work out and test ideas that may or may not come to fruition.  They can be moved to the main map if it is decided to be used.
* The sandbox area has several pasted schematics that may be used in any of the maps.
* Currently released maps should always be left in a releasable state unless otherwise noted.  Do not leave with an obviously partially completed build, signs with notes, template builds, etc.  Get things to as close as decent looking as possible before logging off.  If you absolutely have to leave something looking incomplete, make sure an admin knows.
* If you are working near the bedrock borders, make absolutely sure that there are no holes in the bedrock before logging off.
* Do not use valuable blocks as markers to workout a build.  Use something useless like wool.
* The build server is not a playground to toy around with for your own amusement.  Play on the creative server if you want that.

### WorldEditers

* All WE commands are put into a queueing system to prevent lag and server crashes.
* If use use //undo or //redo, wait until it completes before doing another //undo or //redo.  The queueing system tends to get confused by multiples of this in a row and will wipe your command history.
* Do not WorldEdit in large amounts of valuable blocks, even if it is only temporary.
* Do not screw around with WorldEdit for your own amusement or you may lose the privilege.

### Looters

* Do not include stacks of items in any amount in a chest (not even two in a stack).  Items should be spread around in the chest in single form so players have to make split second decisions while being beat by the horde as to what to take and how much.
* Diamond gear should only be in PvP areas or hard to get areas (lots of spawners, surrounded by obsidian, very un-obvious hiding spots, etc).  Same for enchanted golden apples and enderpearls.
* Do not include enchantment books in chests.  Enchant items instead or leave Bottles of Enchanting.
* No chest should contain a large number of valuables.  The more valuables in the chest, the harder it should be to obtain the contents.


## Commands

### General

| Command | Description |
| ------- | ----------- |
| /jumpto | Sends you to the block you’re looking at.  Can also be done by left clicking with a compass. |
| /thru | Sends you through the wall you’re looking at.  Can also be done by right clicking a compass. |
| /tp <player\> | Teleports you to another player. |
| /speed flight # | Sets your flight speed (1-10).  Do not set to high speeds and zip around the map.  You will crash the server! |
| /speed walk # | Sets your walking speed (1-5). |
| /gm <mode\> | Change your gamemode either via digit or name.<br><br>Gamemodes:<br>0: Survival<br>1: Creative<br>2: Adventure<br>3: Spectator |
| /effect <player\> <effect\> [seconds] [amplifier] | Give yourself a potion effect (most likely night vision).  Do not abuse this by annoying other builders or your perms for this will be revoked. |

### Warps

| Command | Description |
| ------- | ----------- |
| /warp <warp-name\> | Sends you to a warp.  You will only be teleported if you have access to that map. |
| /warp list | Lists all warps. |
| /warp pcreate <name\> | Creates a private warp.  You can create up to 10. |
| /warp delete <name\> | Deletes one of your private warps. |
| /warp update <name\> | Updates the location of your warp. |

### Spawners

| Command | Description |
| ------- | ----------- |
| /ss list | Lists all available mobs. |
| /ss set <mob\> | Sets the spawner you are looking at to the specified mob. |
| /ss give <player\> <mob\> [amount] | Gives a player (most likely yourself) a spawner of the given type. |

### Heads

| Command | Description |
| ------- | ----------- |
| /headsinv | Display heads inventory. |
| /headsinv cat | Display head categories and select. |
| /headsinv search <search-string\> | Searches for a head in the inventory. |
| /myhead | Get your head. |
| /playerhead <player\> | Get a player’s head. |

### Loot Chests

| Command | Description |
| ------- | ----------- |
| /loot link <chest-type\> | Links the chest you are looking at to the given loot chest type.<br><br>Chest types:<br>l2d: Primary chest type. Used in most places.<br>l2d2: Special type of one-time chest.  Do not use.<br>l2d3: Chests with higher value items.  Used in PvP zones and hard to reach areas. |
| /loot unlink | Unlinks a loot chest. |
| /loot help | View all /loot commands.  Generally builders with /loot perms have access to all of the /loot commands. |

### WorldEdit

These perms are available to all builders in the sandbox world to learn WorldEdit.  Only after learning WorldEdit will builders be able to request WorldEdit perms in the main maps.

| Command | Description |
| ------- | ----------- |
| //pos1<br>//pos2 | Set the first and second coordinates of a region.  Can also use the WorldEdit wand (wood axe) to left/right click the positions. |
| //desel | Deselects the selected region. |
| //wand | Get the WorldEdit wand. |
| //undo [#] | Undoes your last change or # of changes. |
| //up 1 | Places a glass block beneath your feet.  Useful for starting a build off the ground. |
| //inset [-h] # | Shrinks a region by # blocks on all sides or only horizontally with the -h flag. |
| //outset [-h] # | Expands a region by # blocks on all sides or only horizontally with the -h flag. |
| //contract # [#] [direction] | Contracts a region by # blocks in the direction specified (north, east, south, west, up, down).  Specifying two numbers will result in the selection contracting by the first number in the direction specified and the second number on the opposite side.  If direction is not specified, it will use the direction you are facing.<br><br>Ex: //contract 4 2 east<br>The west side will shrink eastwards by four blocks and the east side will shrink by two blocks westwards. |
| //expand # [#] [direction] | Same as //contract, except expanding the selection. |
| //shift # [direction] | Shifts the selection by # blocks in the specified direction you specify or are facing (north, east, south, west, up, down). |
| //move # [direction] | Will move the blocks in the selection by # blocks in the specified direction you specify or are facing (north, east, south, west, up, down). |
| //stack # [direction] | Will copy and paste the blocks in the selection # times in the direction you specify or are facing (north, east, south, west, up, down). |
| //replace <current-block\> <new-block\> | Replaces all of the specified blocks currently in the selection with the new block specified. |
| //set <block\> | Sets every block in the selection to the specified block.<br><br>**WARNING**: This command can be very dangerous and cause the server to crash.  Only use it with small selections and use //replace in lieu of //set whenever possible. |
| //faces <block\> | Replaces all faces of the selection with the specified block (vertical and horizontal). |
| //walls <block\> | Replaces only the vertical faces of the selection with the specified block. |
| //sphere <block\> <radius1\>[,radius2][,radius3] | Creates a sphere of the given radius of the specified block. You can also specify all three radii of the sphere (X, Y, Z) |
| //cyl <block\> <radius1\>[,radius2] [height] | Creates a cylinder of the specified block with the given radius and height.  You can also specify both radii of the cylinder (X, Z). |
| //copy | Copies the blocks within the selection.  Where you stand when running the command will be the reference point for pasting. |
| //cut | Cuts the blocks out of the world and copies them.  Where you stand when running the command will be the reference point for pasting. |
| //paste | Pastes the copied blocks.  Where you stand when pasting will represent the same point you stood when copying/cutting. |
| //flip [direction] | Flips the copied selection in the specified direction (north, south, east, west, up, down).  Keep in mind that your reference point for pasting will also flip! |
| //rotate <y-axis\> [x-axis] [z-axis] | Rotates the selection about the given axes.  Keep in mind that your reference point will also rotate! |


### WorldEdit (Brush Perms)

| Command | Description |
| ------- | ----------- |
| //brush sphere [-h] <type\> [radius]  | Use the sphere brush tool. |
| //brush smooth [-n] [radius] [iterations] | Use the smooth brush tool. |
| //mask <mask\> | Set the brush mask. |
