---
title: "General"
linkTitle: "General"
type: docs
weight: 1
robotsdisallow: true
exclude_search: true
---

## Chain of Command

* Head Admin
	* Jr. Admins
		* Senior Moderators
			* Moderators
			* Chat Moderators
	* Architects
		* Builders
	* Community Manager


## Staff Rules

The staff of L2D are representatives of the server.  Players should be able to trust and respect staff.  Staff are still allowed to play as normal players with the following rules:

* No scamming players.
* No stealing from players.
* No verbally fighting with players in the global channels.  Take it to mature.  (And even then, do not go too far....keep it civil).
* No fighting with staff in any player visible channel.  Do not correct a staff member in player visible channels.  Correct them politely in a staff only channel and allow for them to correct themselves in the public chats.
* Treat other staff with respect.  No personal attacks on another staff member.
* No using the staff privileges to obtain an unfair advantage for yourself or other players or staff.
* Other than Head Admin, staff are rule enforcers, not rule interpreters or rule creators.  Do not make up your own rules or punish a player for something just because you do not like it or them.
* What happens in staff chat, stays in staff chat.


## Expectations

Aside from what is listed for each individual rank, the following expectations apply to all staff.

* Staff are not paid.  As such, there is no expectation that you have to be on the server all the time.  You do not need to report that you will be gone for a few days.  Life > Minecraft.
* Don’t assume that just because other staff are on that you don’t have to do anything.  Help each other out so no one staff member is doing all the work.  If a staff member of a higher rank is doing what is supposed to be your job, then you need to be picking it up a bit.  If you have been on for a long time, it is understandable to take a break from the job, just make sure the rest of the staff online at the time know this so there is no resentment amongst the team.
* Staff that are gone for more than a month without notifying an admin may be demoted for inactivity.  This largely depends on how many changes have happened on the server in that time.  Returning staff can be re-added once they catch up with any changes assuming their position has not already been filled.


## Policies

See [Policies]({{< ref "/wiki/general/policies" >}}) for most everything on how the server is run.

## Communications

### In-Game

* All moderation staff have access to the Staff channel (/ch st).
* Moderators have access to the Mod channel (/ch m).
* Admins have access to the Admin channel (/ch a).
* All Architects and Builders have access to the build channel (/ch b).

### Out-of-Game

Staff are not required to use any chat utility outside of Minecraft itself, however, for those interested, we have the following:

* Discord - /discord in game for the invite link


## Perks

* All staff receive the same perks as the Survivor donation rank.
* Staff that leave the staff position on good terms will keep the Survivor rank regardless of their donation status prior to becoming staff.  Staff that leave on poor terms will be put on their original rank prior to becoming staff.


## Teams
### Moderation Team

The moderation team is responsible for keeping the peace in-game.  They deal with unruly players, hackers, and helping out and welcoming players in the game.

### Build Team

The build team consist of the skilled builders responsible for creating the play environment for players.  This includes creating the maps themselves as well as hiding the loot.  Due to the nature of these positions, it is the most strict in terms of following policy and may result in removal from the team if these guidelines are violated.

### Administrative Team

The administrative team are the senior most members of the L2D staff and oversee all aspects of the server.
